/* config-overrides.js */

/**
 * See Issue - [how to change dev server properties in config_overrides?](https://github.com/timarney/react-app-rewired/issues/443)
 */
 module.exports = {
  webpack: function (config, env) {
      // changes to the compilation here - this is the normal function used in react-app-rewired.
      return config;
  },
  devServer: function (configFunction) {
      return function (proxy, allowedHost) {
          // Create the default config by calling configFunction with the proxy/allowedHost parameters
          const config = configFunction(proxy, allowedHost);
          // Change the dev server's config here...
          // Then return your modified config.
          return config;
      }
  }
};
